<?php

class barang extends CI_Controller
{

  function index()
  {
    $this->load->model('model_barang');
    $judul          = "Daftar Barang lempar controller";
    $data['judul']  = $judul;
    $data['barang'] = $this->model_barang->list_barang()->result();
    $this->load->view('list_barang',$data);
/* sample pengambilan data
    $barang = $this->db->get('barang');
    print_r($barang);
    die();
*/
  }

  function insert()
  {
    $this->load->model('model_barang');
    $judul          = "Input Barang";
    $data['judul']  = $judul;
    $data['barang'] = $this->model_barang->list_barang()->result();
    $this->load->view('input_barang',$data);
  }

  function insert_brg()
  {
    //hasil inputan dari view
    //$id_barang = $this->input->post('kode_brg');
    //echo $id_barang;
    //cara insert dari code CodeIgniter
    $data_barang = array(
                          'id_brg'      => $this->input->post('kode_brg'),
                          'nama_brg'    => $this->input->post('nama_brg'),
                          'harga_brg'   => $this->input->post('harga'),
                          'stock'       => $this->input->post('stock')
                        );
    // Masuk ke DB
    $this->db->insert('barang',$data_barang);
    //autoload URL harus di pasang
    redirect('barang');
  }

  function edit_brg()
  {
    $this->load->model('model_barang');
    $id_brg         = $this->uri->segment(3);
    $judul          = "Edit Produk $id_brg ";
    $data['judul']  = $judul;
    //$produk = $this->model_barang->get_brg($id_brg)->row_array(); DALAM BENTUK ARRAY
    //$produk = $this->model_barang->get_brg($id_brg)->result(); DALAM BENTUK OBJEK
    $data['produk'] = $this->model_barang->get_brg($id_brg)->row_array();
    $this->load->view('edit_barang',$data);
  }

  function edit_brg_save()
  {
    $id           = $this->input->post('id_brg');
    $data_barang  = array(
                          'id_brg'      => $this->input->post('kode_brg'),
                          'nama_brg'    => $this->input->post('nama_brg'),
                          'harga_brg'   => $this->input->post('harga'),
                          'stock'       => $this->input->post('stock')
                        );
    // Masuk ke DB
    $this->db->where('id_brg',$id);
    $this->db->update('barang',$data_barang);
    //autoload URL harus di pasang
    redirect('barang');
  }

  function delete()
  {
    $id_brg         = $this->uri->segment(3);
    $this->db->where('id_brg',$id_brg);
    $this->db->delete('barang');
    redirect('barang');

  }

}

?>
